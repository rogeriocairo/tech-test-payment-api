﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Threading.Tasks;
using tech_test_payment_api.Core.Interface.Services;
using tech_test_payment_api.Core.Model;

namespace tech_test_payment_api.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class SaleController : ControllerBase
    {
        private readonly ISaleService _saleService;

        public SaleController(ISaleService saleService)
        {
            _saleService = saleService;
        }

        [HttpPost("RegistrarVenda")]
        public async Task<IActionResult> RegisterSale([FromBody] Venda vendedor)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    var _response = await _saleService.RegisterAsync(vendedor);
                    return StatusCode(_response.StatusCode, _response);
                }
                else
                {
                    return StatusCode(StatusCodes.Status400BadRequest, "Venda não efetuada");
                }
            }
            catch (Exception ex)
            {
                return StatusCode(StatusCodes.Status400BadRequest, ex.Message);
            }
        }

        [HttpGet("BuscarVenda")]
        public async Task<IActionResult> GetSale(int codigoVenda)
        {
            try
            {
                var _response = await _saleService.GetAsync(codigoVenda);
                return StatusCode(_response.StatusCode, _response);
            }
            catch (Exception ex)
            {
                return StatusCode(StatusCodes.Status400BadRequest, ex.Message);
            }
        }

        [HttpPost("AtualizarVenda")]
        public async Task<IActionResult> UpdateSale([FromBody] Venda vendedor)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    var _response = await _saleService.UpdateAsync(vendedor);
                    return StatusCode(_response.StatusCode, _response);
                }
                else
                {
                    return StatusCode(StatusCodes.Status400BadRequest, "Venda não atualizada!");
                }
            }
            catch (Exception ex)
            {
                return StatusCode(StatusCodes.Status400BadRequest, ex.Message);
            }
        }
    }
}
