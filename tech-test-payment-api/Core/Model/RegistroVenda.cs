﻿using System.Collections.Generic;

namespace tech_test_payment_api.Core.Model
{
    public class RegistroVenda
    {
        public Vendedor Vendedor { get; set; }
        public List<Itens> Itens { get; set; }
    }
}
