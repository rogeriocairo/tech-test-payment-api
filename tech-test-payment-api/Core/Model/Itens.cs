﻿namespace tech_test_payment_api.Core.Model
{
    public class Itens
    {

        public int Id { get; set; }
        public string Descricao { get; set; }
        public int Quantidade { get; set; }
        public decimal Valor { get; set; }
    }
}
