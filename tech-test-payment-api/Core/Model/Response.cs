﻿namespace tech_test_payment_api.Core.Model
{
    public class Response<T> where T : class
    {
        public Response(T data, bool success = true, int statusCode = 200, string message = "")
        {
            Data = data;
            Success = success;
            StatusCode = statusCode;
            Message = message;
        }

        public T Data { get; set; }

        public bool Success { get; set; }

        public int StatusCode { get; set; }

        public string Message { get; set; }
    }
}


