﻿using System.Collections.Generic;
using System;

namespace tech_test_payment_api.Core.Model
{
    public class Venda
    {
        public int Id { get; set; }
        public DateTime Data { get; set; }
        public string Status { get; set; }
        public Vendedor Vendedor { get; set; }
        public List<Itens> Itens { get; set; }
    }
}
