﻿using System.Threading.Tasks;
using tech_test_payment_api.Core.Model;

namespace tech_test_payment_api.Core.Interface.Services
{
    public interface ISaleService
    {
        public Task<Response<Venda>> RegisterAsync(Venda sale);

        public Task<Response<Venda>> GetAsync(int codigoVenda);

        public Task<Response<Venda>> UpdateAsync(Venda sale);        
    }
}
