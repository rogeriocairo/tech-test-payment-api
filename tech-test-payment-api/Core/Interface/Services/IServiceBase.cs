﻿namespace Stocks.Core.Interface.Services
{
    public interface IServiceBase<TEntity> where TEntity : class
    {
        public void Add(TEntity _entity);
    }
}
