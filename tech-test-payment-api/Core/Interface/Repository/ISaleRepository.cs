﻿using System.Threading.Tasks;
using tech_test_payment_api.Core.Model;

namespace tech_test_payment_api.Core.Interface.Repository
{
    public interface ISaleRepository
    {
        Task<Venda> SaveAsync(Venda sale);

        public Task<Venda> GetAsync(int codigoVenda);

        public Task<Venda> UpdateSaleAsync(Venda sale);
    }
}
