﻿using Dapper;
using System.Threading.Tasks;

namespace Stocks.Api.Core.Interface.Repository
{
    public interface IRepositoryBase<TEntity> where TEntity : class
    {        
        Task<TEntity> QueryAsync(string procedure, DynamicParameters parameters);

        Task<int> ExecAsync(string procedure, DynamicParameters parameters);
    }
}