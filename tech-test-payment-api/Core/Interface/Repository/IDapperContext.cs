﻿using Microsoft.Data.SqlClient;

namespace Stocks.Api.Core.Interface.Repository
{
    public interface IDapperContext
    {
        SqlConnection ObterConexao();
    }
}
