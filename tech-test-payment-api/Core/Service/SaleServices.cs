﻿using Microsoft.AspNetCore.Http;
using System.Threading.Tasks;
using tech_test_payment_api.Core.Interface.Repository;
using tech_test_payment_api.Core.Interface.Services;
using tech_test_payment_api.Core.Model;

namespace tech_test_payment_api.Core.Service
{
    public class SaleServices : ISaleService
    {
        private readonly ISaleRepository _saleRepository;

        public SaleServices(ISaleRepository saleRepository)
        {
            _saleRepository = saleRepository;
        }

        public async Task<Response<Venda>> GetAsync(int  codigoVenda)
        {
            if (codigoVenda <= 0)
                return new Response<Venda>(default, false, StatusCodes.Status400BadRequest, "Informe o código da venda!");

            var _sale = await _saleRepository.GetAsync(codigoVenda);

            return new Response<Venda>(_sale, true, StatusCodes.Status200OK, "");
        }

        public async Task<Response<Venda>> RegisterAsync(Venda venda)
        {
            if (venda.Itens.Count == 0)
            {
                return new Response<Venda>(default, false, StatusCodes.Status400BadRequest, "A venda deve ter pelo menos 1 item.");
            }

            var _sale = await _saleRepository.SaveAsync(venda);

            if (_sale != null)
            {
                return new Response<Venda>(_sale, true, StatusCodes.Status200OK, "Venda Registrada!");
            }

            return new Response<Venda>(default, false, StatusCodes.Status400BadRequest, "Venda não efetuada!");
        }

        public async Task<Response<Venda>> UpdateAsync(Venda venda)
        {
            if (venda.Itens.Count == 0)
            {
                return new Response<Venda>(default, false, StatusCodes.Status400BadRequest, "A venda deve ter pelo menos 1 item.");
            }

            var _sale = await _saleRepository.GetAsync(venda.Id);            

            if (_sale != null)
            {
                if (!IsValidTransition(_sale.Status, venda.Status))
                {
                    return new Response<Venda>(_sale, false, StatusCodes.Status400BadRequest, "Transição de status não permitida!");
                }

                var _saleUpdate = await _saleRepository.UpdateSaleAsync(venda);

                return new Response<Venda>(_saleUpdate, true, StatusCodes.Status200OK, "Venda Atualizada!");
            }

            return new Response<Venda>(default, false, StatusCodes.Status400BadRequest, "Venda não pode ser atualizada!");
        }

        private bool IsValidTransition(string currentStatus, string newStatus)
        {
            return currentStatus switch
            {
                "Aguardando pagamento" => newStatus == "Pagamento Aprovado" || newStatus == "Cancelada",
                "Pagamento Aprovado" => newStatus == "Enviado para transportadora" || newStatus == "Cancelada",
                "Enviado para Transportador" => newStatus == "Entregue",
                _ => false,
            };
        }
    }
}
