﻿using Dapper;
using Stocks.Api.Core.Interface.Repository;
using System.Data;
using System.Threading.Tasks;
using static Dapper.SqlMapper;

namespace tech_test_payment_api.Core.Repository
{
    public class RepositoryBase<TEntity> : IRepositoryBase<TEntity> where TEntity : class
    {
        private readonly IDapperContext _dataContext;

        public RepositoryBase(IDapperContext dapperContext)
        {
            _dataContext = dapperContext;
        }

        public async Task<int> ExecAsync(string procedure, DynamicParameters parameters)
        {
            using var dbConnection = _dataContext.ObterConexao();
            await dbConnection.OpenAsync();

            var _result = await dbConnection.ExecuteAsync(
                            procedure, parameters, commandType: CommandType.StoredProcedure);

            return _result;
        }

        public async Task<TEntity> QueryAsync(string procedure, DynamicParameters parameters)
        {
            using var dbConnection = _dataContext.ObterConexao();
            await dbConnection.OpenAsync();

            var _result = await dbConnection.QueryFirstOrDefaultAsync<TEntity>(
                            procedure, parameters, commandType: CommandType.StoredProcedure);

            return _result ?? default;
        }        
    }
}