﻿using Dapper;
using Stocks.Api.Core.Interface.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using tech_test_payment_api.Core.Interface.Repository;
using tech_test_payment_api.Core.Model;

namespace tech_test_payment_api.Core.Repository
{
    public class SaleRepository : ISaleRepository
    {
        private readonly IRepositoryBase<Venda> _repositoryBase;

        private readonly List<Venda> _vendas = new List<Venda>();

        public SaleRepository(IRepositoryBase<Venda> repositoryBase)
        {
            _repositoryBase = repositoryBase;

            _vendas.Add(new Venda
            {
                Id = 1,
                Data = DateTime.Now,
                Status = "Aguardando pagamento",
                Vendedor = new Vendedor
                {
                    Id = 1,
                    Cpf = "12345678901",
                    Nome = "João Silva",
                    Email = "joaosilva@email.com",
                    Telefone = "(11) 9999-9999"
                },
                Itens = new List<Itens>
                {
                    new Itens
                    {
                        Id = 1,
                        Descricao = "Produto 1",
                        Quantidade = 1,
                        Valor = 100.00M
                    },
                }
            });
        }

        public async Task<Venda> SaveAsync(Venda sale)
        {
            sale.Id++;
            sale.Data = DateTime.Now;
            sale.Status = "Aguardando pagamento";

            return sale;

            //Persistência
            //var _param = new DynamicParameters();
            //var _result = await _repositoryBase.ExecAsync("spSaleInsert", _param);
            //if (_result == null)
            //{
            //    return false;
            //}

            //return true;
        }

        public async Task<Venda> GetAsync(int codigoVenda)
        {
            var _sale = _vendas.First();
            _sale.Status = "Aguardando pagamento";

            return _sale;

            //Persistência
            //var _param = new DynamicParameters();
            //var _result = await _repositoryBase.QueryAsync("spSaleGetById", _param);
            //if (_result == null)
            //{
            //    return false;
            //}
            //return true;
        }

        public async Task<Venda> UpdateSaleAsync(Venda sale)
        {
            var _result = _vendas.Find(x => x.Id == sale.Id);
            _result.Status = sale.Status;            
            return _result;

            //Persistência
            //var _param = new DynamicParameters();
            //var _result = await _repositoryBase.ExecAsync("spSaleUpdate", _param);
            //if (_result == null)
            //{
            //    return false;
            //}
            //return true;
        }
    }
}
