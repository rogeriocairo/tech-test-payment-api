using AutoBogus;
using Moq;
using System.Threading.Tasks;
using tech_test_payment_api.Core.Interface.Repository;
using tech_test_payment_api.Core.Model;
using tech_test_payment_api.Core.Service;
using Xunit;

namespace tech_test_payment_api.test
{
    public class SaleTest
    {
        private readonly MockRepository mockRepository;
        private readonly Mock<ISaleRepository> mockSaleRepository;

        public SaleTest()
        {
            mockRepository = new MockRepository(MockBehavior.Strict);
            mockSaleRepository = new Mock<ISaleRepository>();
        }

        private SaleServices CreateService()
        {
            return new SaleServices(mockSaleRepository.Object);
        }

        [Theory]
        [InlineData(200, 1)]
        [InlineData(400, 0)]
        public async Task SaveAsync_RegisterSale_RetornaVenda(int codeReturn, int item)
        {
            // Arrange
            var _service = CreateService();
            var _sale = new AutoFaker<Venda>().Generate();

            if (item == 0)
            {
                _sale.Itens.Clear();
            }

            mockSaleRepository
            .Setup(r => r.SaveAsync(It.IsAny<Venda>()))
            .ReturnsAsync(_sale);

            // Act
            var _result = await _service.RegisterAsync(_sale);

            // Assert
            Assert.Equal(codeReturn, _result.StatusCode);
            mockRepository.VerifyAll();
        }

        [Fact]
        public async Task GetSaleAsync_GetSale_RetornaVenda()
        {
            // Arrange
            var _service = CreateService();
            var _sale = new AutoFaker<Venda>()
                            .RuleForType(typeof(int), f => f.Random.Int(1, int.MaxValue))
                            .Generate();

            mockSaleRepository
            .Setup(r => r.GetAsync(It.IsAny<int>()))
            .ReturnsAsync(_sale);

            // Act
            var _result = await _service.GetAsync(_sale.Id);

            // Assert
            Assert.True(_result.Success);
            mockRepository.VerifyAll();
        }

        [Theory(DisplayName = "Testes para o status Pagamento Aprovado")]
        [InlineData("Aguardando pagamento", false)]
        [InlineData("Pagamento Aprovado", true)]
        public async Task UpdateSaleAsync_TestsAP_ReturnSale(string status, bool isSucess)
        {
            // Arrange
            var _service = CreateService();
            var _saleCurrent = new AutoFaker<Venda>()
                            .RuleForType(typeof(int), f => f.Random.Int(1, int.MaxValue))
                            .Generate();
            var _saleNew = new AutoFaker<Venda>()
                            .RuleForType(typeof(int), f => f.Random.Int(1, int.MaxValue))
                            .Generate();

            _saleCurrent.Status = "Aguardando pagamento";
            _saleNew.Status = status;

            mockSaleRepository
            .Setup(r => r.UpdateSaleAsync(It.IsAny<Venda>()))
            .ReturnsAsync(_saleNew);

            mockSaleRepository
           .Setup(r => r.GetAsync(It.IsAny<int>()))
           .ReturnsAsync(_saleCurrent);

            // Act
            var _result = await _service.UpdateAsync(_saleNew);

            // Assert
            Assert.Equal(isSucess, _result.Success);
            mockRepository.VerifyAll();
        }
    }
}

